<?php
 
class Eyemagine_Test_Model_Observer
{
    public function insertBlock($observer)
    {
        /** @var $_block Mage_Core_Block_Abstract */
        /*Get block instance*/
        $_block = $observer->getBlock();
        /*get Block type*/
        $name = $_block->getNameInLayout();
        
        $layout = Mage::getSingleton('core/layout');
       /*Check block type*/
        if ($name == 'product.description') {
            /*Clone block instance*/
            $_child = $layout->createBlock('eyemagine/test');
            
            $_block->setChild('child', $_child);
            
            /*set our template*/
            $_child->setTemplate('test/at.phtml');
        }
    }
}

