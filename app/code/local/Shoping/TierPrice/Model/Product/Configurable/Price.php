<?php
/**
 * Rewrite Configurable Product Model which is responsible for price formation
 */
class Shoping_TierPrice_Model_Product_Configurable_Price extends Mage_Catalog_Model_Product_Type_Configurable_Price
{
    /**
     * Get product final price
     *
     * @param   double $qty
     * @param   Mage_Catalog_Model_Product $product
     * @return  double
     */
    public function getFinalPrice($qty=null, $product)
    {
        if (is_null($qty) && !is_null($product->getCalculatedFinalPrice())) {
            return $product->getCalculatedFinalPrice();
        }
        
        // Get count of the same configurable products if exists
        $count = $this->_getCountItemsInCheckout($product);
        
        $basePrice = $this->getBasePrice($product, $count);
        //$basePrice = $this->getBasePrice($product, $qty);

        $finalPrice = $basePrice;
        $product->setFinalPrice($finalPrice);
        Mage::dispatchEvent('catalog_product_get_final_price', array('product' => $product, 'qty' => $qty));
        $finalPrice = $product->getData('final_price');

        $finalPrice += $this->getTotalConfigurableItemsPrice($product, $finalPrice);
        $finalPrice += $this->_applyOptionsPrice($product, $qty, $basePrice) - $basePrice;
        $finalPrice = max(0, $finalPrice);

        $product->setFinalPrice($finalPrice);

        return $finalPrice;
    }
    
    // Getting quantity of the same configurable products in the checkout-session
    protected function _getCountItemsInCheckout($product)
    {
        $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();

        $qty = array();
     
        foreach($items as $item) {
            if($item->getProductId() == $product->getId()){
                $qty[] = $item->getQty();
            }
        }

        // Get count of the same configurable products
        $count = array_sum($qty);
        
            
        return $count;
    }
}
