<?php
/* 
 * Change final price of configurable products
 */
class Shoping_TierPrice_Model_Observer
{
    /**
     * Applies the tier price discount
     * @param   Varien_Event_Observer $observer
     * @return  Shoping_TierPrice_Model_Observer
     */
    public function addDiscount($observer)
    {
        $event = $observer->getEvent();
        $product = $event->getProduct();  
        
        if($product->getTypeId() == 'configurable'){
            // Getting price
            $price = $product->getPrice();

            //Zend_Debug::dump($product->getTypeId(), 'product_id: ' . $product->getId() . '; product name - ' . $product->getName() . '; product type - ');
            
            // Return Mage_Catalog_Model_Resource_Product_Type_Configurable_Attribute_Collection
            $attributes = $product->getTypeInstance(true)->getConfigurableAttributes($product);

            // Get Model of the stock items
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId())->getData();

            // Gets all quotes in session
            $items = Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();

            $qty = array();
     
            foreach($items as $item) {
                if($item->getProductId() == $product->getId()){
                    $qty[] = $item->getQty();
                }
            }

            // Get count of similar configurable products
            $count = array_sum($qty);
            // Getting tier price, which defines by admin
            //$specialPrice = $product->getTierPrice($count);

            // Setting new price
            //$product->setFinalPrice($specialPrice);
           
            // Getting total items in Cart
            //$count = Mage::helper('checkout/cart')->getSummaryCount();
        }
        return $this;
    }
}
