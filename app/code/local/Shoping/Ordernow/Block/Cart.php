<?php
/* 
 * Block 
 */
class Shoping_Ordernow_Block_Cart extends Mage_Checkout_Block_Cart_Abstract
{
    /**
     * Get Action URL
     * @return string
     */
    public function getOrderActionUrl()
    {
        return Mage::getBaseUrl() . 'ordernow/buy/buynow';
    }
}