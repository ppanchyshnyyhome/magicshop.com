<?php

/* 
 * Controller for Ordernow Module
 */

class Shoping_Ordernow_BuyController extends Mage_Core_Controller_Front_Action
{
    /**
     * Create Order
     */
    public function buynowAction()
    {
        
        
        $session = Mage::getSingleton('checkout/session');
        $helper  = Mage::helper('ordernow');
        $post    = $this->getRequest()->getPost();
        $quote   = $session->getQuote();

//        $helper->createOrder($quote, $post);  
        $website = Mage::getSingleton('customer/session')->getWebsiteId();
        
        $customerName = $this->getCustomerName($post['name']);
        
        // Check the customers
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            // for registered customers
            $customer = Mage::getModel('customer/customer')
                    ->setWebsiteId($website)
                    ->loadByEmail($post['email']);
            $quote->assignCustomer($customer);
        } else {
            // for guest orders only:
            $quote->setCustomerEmail($post['email'])
             ->setFirstname($customerName[0])
             ->setLastname($customerName[1])
             ->setEmail($post['email`']);
        }
        

    $customerAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling(); 
        //if we have a default billing addreess, try gathering its values into variables we need
    if ($customerAddressId){ 
        $address     = Mage::getModel('customer/address')->load($customerAddressId);
        $street      = $address->getStreet();
        $city        = $address->getCity();
        $postcode    = $address->getPostcode();
        $phoneNumber = $address->getTelephone();
        $countryId   = $address->getCountryId();
        $regionId    = $address->getRegionId();
    // otherwise, setup some custom entry values so we don't have a bunch of confusing un-descriptive orders in the backend
    }else{
        $address     = 'No address';
        $street      = 'No street';
        $city        = 'No City';
        $postcode    = 'No post code';
        $phoneNumber = $post['telephone'];
        $countryId   = 'No country';
        $regionId    = 'No region';        
    }
        
        
    //Low lets setup a shipping / billing array of current customer's session
    $addressData = array(
        'firstname'  => $customerName[0],
        'lastname'   => $customerName[1],
        'street'     => $street,
        'city'       => $city,
        'postcode'   => $postcode,
        'telephone'  => $phoneNumber,
        'country_id' => $countryId,
        'region_id'  => $regionId
    );
    
    
    
        $billingAddress  = $quote->getBillingAddress()->addData($addressData);
        $shippingAddress = $quote->getShippingAddress()->addData($addressData);
    

        $shippingAddress->setCollectShippingRates(true)->collectShippingRates()
                    ->setShippingMethod('flatrate_flatrate')
                    ->setPaymentMethod('checkmo');

        $quote->getPayment()->importData(array('method' => 'checkmo'));

        $quote->collectTotals()->save();
      
        $service = Mage::getModel('sales/service_quote', $quote)
                ->submit();
        $increment_id = $service->getOrder()->getRealOrderId();
        
        
        $this->_redirect('checkout/onepage/success');
    }
    
    
    public function getCustomerName($name)
    {
        $name = explode(' ', $name);
        
        return $name;
    }
    

}