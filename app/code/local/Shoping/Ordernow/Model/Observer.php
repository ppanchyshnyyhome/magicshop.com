<?php

/** 
 * Observer for Ordernow Module
 */

class Shoping_Ordernow_Model_Observer
{
    /**
     * Setting Child Block (Order Now Form) to Checkout Blocks
     * @event  
     * @param Varien_Event_Observer $observer
     */
    
    public function setChildToLayout(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        $layout = Mage::getSingleton('core/layout');
        
        //Check the other blocks to find our block
        if ($block->getNameInLayout() == 'checkout.cart.widget') {
            $child = $layout->createBlock('ordernow/cart')
                            ->setTemplate('ordernow/order_form.phtml');

            $block->append($child);
        }
    }
}