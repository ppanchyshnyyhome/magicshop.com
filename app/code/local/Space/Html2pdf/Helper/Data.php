<?php

/** 
 * Helper class
 * 
 * @category  
 * @package   Web4pro
 * @author    Web4pro
 */

class Space_Html2pdf_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * 
     * @param Mage_Sales_Model_Order $order
     */
    public function getInvoices($order)
    {
        $invoices = $order->getInvoiceCollection();
        
        foreach ($invoices as $invoice){
            $invoiceIds[] = $invoice->getId();
        }
        
        return implode('/', $invoiceIds);
    }
}