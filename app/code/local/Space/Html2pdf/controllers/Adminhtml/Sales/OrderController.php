<?php

/** 
 * Main Adminhtml Controller
 * 
 * @category  Space_Html2pdf
 * @package   Web4pro
 * @author    Web4pro
 */

class Space_Html2pdf_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Controller_Action
{
    
    public function printAction()
    {
        require_once Mage::getBaseDir().DS.'html2pdf_v4.03'.DS.'html2pdf.class.php';
        $orderId   = $this->getRequest()->getParam('order_id');
        $invoiceId = $this->getRequest()->getParam('invoice_id');
        
        $order   = Mage::getModel('sales/order')->load($orderId);
        $invoice = Mage::getModel('sales/order_invoice')->load($invoiceId);
        
        $content = $this->getLayout()
                        ->createBlock('html2pdf/adminhtml_order_info')
                        ->setOrder($order)
                        ->setInvoice($invoive)
                        ->setTemplate('html2pdf/order_info.phtml')
                        ->toHtml();
   
        try
        {
            $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 0);
            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
            $html2pdf->Output('OrderDataSheet.pdf');
        }
        catch(HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }
}