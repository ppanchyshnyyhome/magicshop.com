<?php

/** 
 * Observer class
 * @methods  addButtonToOrderViewBlock() "Adds "Print order" button to Order View Block"
 * 
 * @category  Space_Html2pdf
 * @package   Web4pro
 * @author    Web4pro
 */

class Space_Html2pdf_Model_Observer 
{
    /**
     * Adds "Print order" button to Order View Block
     * if Order Status is Complete
     * 
     * @event 
     * @param Varien_Event_Observer $observer
     */
    public function addButtonToOrderViewBlock($observer)
    {
        $block  = $observer->getBlock();
        $name   = $block->getNameInLayout();
        $helper = Mage::helper('html2pdf');
        
        if($name == 'sales_order_edit'){
            $order       = $block->getOrder();
            $invoice     = $order->getInvoice();
            $orderStatus = $order->getStatus();
            
            if ($orderStatus == $order::STATE_COMPLETE) {
                $block->addButton('order_print', array(
                    'label'     => $helper->__('Print Order'),
                    'class'     => 'go',
                    'onclick'   => 'setLocation(\'' . 
                                   $block->getUrl('*/*/print', array('invoice_id' => $helper->getInvoices($order))) 
                                   . '\')'
                ));
            }
        }
    }
}