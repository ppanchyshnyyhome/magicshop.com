<?php
/** Main Controller for Shares Module in admin panel*/
class Space_Shares_Adminhtml_SharesController extends Mage_Adminhtml_Controller_Action
{
    // Set actine menu and load layout
    protected function _initAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('space_shares');
    }
    
    // Create Block to display a list of promo-shares in the backend
    public function indexAction()
    {   
        $this->_title($this->__('Shares'));
        
        $this->_initAction();
        
        $contentBlock = $this->getLayout()->createBlock('shares/adminhtml_shares');
        $this->_addContent($contentBlock);
        
        $this->renderLayout();
    }
    
    /*
     * Action for Create a new promo-share
     */
    public function newAction()
    {             
        $this->_forward('edit'); 
    }
    
    /*
     * Action to edit citations
     */
    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        
        $this->_title($this->__('Shares'));
        if($id){
            $this->_title($this->__('Edit Promo-Share'));
        } else{
            $this->_title($this->__('New Promo-Share'));
        }
       
        $model = Mage::getModel('shares/shares')->load($id);
        // Registration loaded id of the Share to be edited
        Mage::register('current_share', $model);
        
        // Load layout and block for edit
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('shares/adminhtml_shares_edit'))
             ->_addLeft($this->getLayout()->createBlock('shares/adminhtml_shares_edit_tabs'));   
        $block = $this->getLayout()->getBlock('catalog.wysiwyg.js');
        $this->renderLayout();
    }
    
    /*
     * Action to save the edited and the new Promo-Shares
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('shares/shares');
                $helper = Mage::helper('shares');
                
                // Retrieve all Shared products from Serializer
                if ($productIds = $this->getRequest()->getParam('product_ids', null)) {
                    // Setting to the data
                    $explode = explode("&", $productIds);
                    $implode = implode(",", $explode);

                    $data['products'] = $implode;
                }

                // Set and Saving Data
                $model->setData($data)->setId($this->getRequest()->getParam('id'));               
                if(!$model->getCreated()){
                    $model->setCreated(now());
                }
                
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Promo-Share was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }
    
    // Action for deleting promo-shares
    public function deleteAction()
    {
        $shareId   = $this->getRequest()->getParam('id', false);
        $session    = Mage::getSingleton('adminhtml/session');

        try {
            Mage::getModel('shares/shares')->setId($shareId)
                ->delete();

            $session->addSuccess(Mage::helper('shares')->__('The Promo-Share has been deleted'));
            
            $this->getResponse()->setRedirect($this->getUrl('*/*/'));

            return;
        } catch (Mage_Core_Exception $e) {
            $session->addError($e->getMessage());
        } catch (Exception $e){
            $session->addException($e, Mage::helper('shares')->__('An error occurred while deleting this share.'));
        }

        $this->_redirect('*/*/edit/',array('id'=>$shareId));
    }
    
    // Action for Mass Deleting
    public function massDeleteAction()
    {
        $sharesIds = $this->getRequest()->getParam('shares');
        $session    = Mage::getSingleton('adminhtml/session');

        if(!is_array($sharesIds)) {
             $session->addError(Mage::helper('adminhtml')->__('Please select share(s).'));
        } else {
            try {
                foreach ($sharesIds as $shareId) {
                    $model = Mage::getModel('shares/shares')->load($shareId);
                    $model->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) have been deleted.', count($sharesIds))
                );
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
            } catch (Exception $e){
                $session->addException($e, Mage::helper('adminhtml')->__('An error occurred while deleting record(s).'));
            }
        }

        $this->_redirect('*/*/' . $this->getRequest()->getParam('ret', 'index'));
    }

    // Action for Mass Updating Statuses
    public function massUpdateStatusAction()
    {
        $sharesIds = $this->getRequest()->getParam('shares');

        $session    = Mage::getSingleton('adminhtml/session');

        if(!is_array($sharesIds)) {
             $session->addError(Mage::helper('adminhtml')->__('Please select share(s).'));
        } else {
            /* @var $session Mage_Adminhtml_Model_Session */
            try {
                $status = $this->getRequest()->getParam('status');
                foreach ($sharesIds as $shareId) {
                    $model = Mage::getModel('shares/shares')->load($shareId);
                    $model->setStatusId($status)
                          ->save();
                }
                $session->addSuccess(
                    Mage::helper('adminhtml')->__('Total of %d record(s) have been updated.', count($sharesIds))
                );
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
            } catch (Exception $e) {
                $session->addException($e, Mage::helper('adminhtml')->__('An error occurred while updating the selected share(s).'));
            }
        }

        $this->_redirect('*/*/');
    }
    
     /*
     * Product grid on the Tab 'Attached Products'
     */   
    public function productsAction() 
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('products.grid')
                ->setSelectedProducts($this->getRequest()->getPost('products', null));
        $this->renderLayout();
    }
    
    /*
     * Product grid on the Tab 'Attached Products' when reloads page via AJAX
     */
    public function productGridAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('products.grid')
                // Pass list of selected items
                ->setSelectedProducts($this->getRequest()->getPost('products', null)); 
        $this->renderLayout();
    }
}
