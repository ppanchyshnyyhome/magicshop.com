<?php
// Main Index controller for Frontend
class Space_Shares_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
    * Index action which shows grid of promo-shares
    */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    
    /**
    * Action for viewing current Promo-Share
    */
    public function viewAction()
    {
        if ($this->_checking()) {
            $this->loadLayout();
            $this->renderLayout();
        } else {
            // Forwarding to the '404 page'
            $this->_forward('noRoute');
        }
    }
    
    // Checking: If transition was from the admin panel or from the frontend?
    protected function _checking() {
        $id = $this->getRequest()->getParam('share_id');
        $referer = $this->getRequest()->getHeader('referer');
        // Get Model collection of Current Share
        $share = Mage::getModel('shares/shares')
                ->getCollection()
                ->addFieldToFilter('share_id', $id);

        $statusId = '';
        foreach ($share as $status) {
            $statusId = $status->getStatusId();
        }
        // Checking status of Promo-Share and referer page
        if (($statusId == Mage::helper('shares')->activeStatus) || strpos($referer,'admin/shares') !== false){
            return $share;
        } else {
            return false;
        }
    }
}