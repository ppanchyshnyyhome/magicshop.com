<?php
// Helper for Shares Module 
class Space_Shares_Helper_Data extends Mage_Core_Helper_Abstract
{
    // Product Collection property
    protected $_productCollection;
    
    //Get Active Status
    public $activeStatus = Space_Shares_Model_Shares::STATUS_ACTIVE;


    /**
     * Get Share statuses with their codes
     *
     * @return array
     */
    public function getShareStatuses()
    {
        return array(
            Space_Shares_Model_Shares::STATUS_ACTIVE     => $this->__('Active'),
            Space_Shares_Model_Shares::STATUS_INACTIVE   => $this->__('Inactive'),
        );
    }
    
    /**
     * Get Share statuses as option array
     *
     * @return array
     */
    public function getShareStatusesOptionArray()
    {
        $result = array();
        foreach ($this->getShareStatuses() as $k => $v) {
            $result[] = array('value' => $k, 'label' => $v);
        }

        return $result;
    }
    
    /**
     * Return product collection to be displayed in view action
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection() {
        
        $id = Mage::app()->getRequest()->getParam('share_id');
        
        $shareCollection = Mage::getModel('shares/shares')
                ->getCollection()
                ->addFieldToFilter('share_id', $id);
        
        $products = array();
       
        foreach ($shareCollection as $productlist) {
            $products = $productlist->getProducts();
        }
        $prodIds = explode(",", $products);

        if (is_null($this->_productCollection)) {
            $collection = Mage::getModel('catalog/product')
                ->getCollection('*')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', array('in' => $prodIds));
            //Mage::getModel('catalog/layer')->prepareProductCollection($collection);
            $this->_productCollection = $collection;
        }   
        return $collection;
    }
    
    // Filtering WYSIWYG variables and widgets for rendering via CMS Module
    public function filtering($html)
    {
        $helper = Mage::helper('cms');
        $processor = $helper->getBlockTemplateProcessor();

        return $processor->filter($html);
        
    }
}