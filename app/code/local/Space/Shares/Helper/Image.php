<?php
/**
 * Helper class for resizing images
 */
class Space_Shares_Helper_Image extends Varien_Data_Form_Element_Image
{
    // Directory for Saving images
    protected $_imagePath = 'promotions';
    
    // Directory for resized images
    protected $_resizeImgDir = 'resized';
    
    
    // Resizing Image
    public function resizeImage($imageUrl, $width=NULL, $height=NULL)
    {
         // Create folder
        if(!file_exists("./media/" . $this->_imagePath . DS . $this->_resizeImgDir)){
            mkdir("./media/" . $this->_imagePath . DS . $this->_resizeImgDir,0777);
        }     

         // Get image name
        $imageName = substr(strrchr($imageUrl,"/"),1);

         // Resized image path (media/promotions/resized/IMAGE_NAME)
        $imageResized = Mage::getBaseDir('media').DS."promotions".DS."resized".DS.$imageName;

         // Changing image url into direct path
        $dirImg = Mage::getBaseDir().str_replace("/",DS,strstr($imageUrl,'/media'));

         // If resized image doesn't exist, save the resized image to the resized directory
        if (!file_exists($imageResized)&&file_exists($dirImg)){
            $imageObj = new Varien_Image($dirImg);
            $imageObj->constrainOnly(TRUE);
            $imageObj->keepAspectRatio(TRUE);
            $imageObj->keepFrame(FALSE);
            if($width == NULL && $height == NULL) {
		$width  = 200;
		$height = 200; 
            }
            $imageObj->resize($width, $height);
            $imageObj->save($imageResized);
        }

        return $newImageUrl = Mage::getBaseUrl('media')."promotions/resized/".$imageName;
    }
    
}

