<?php
/**
 * Install script
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

// Create Main Table for Promo-Shares
$tableShares = $installer->getTable('shares/shares_maintable');

$installer->getConnection()->dropTable($tableShares);
$table = $installer->getConnection($tableShares)
    ->newTable($tableShares)
    ->addColumn('share_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Share id')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
    ), 'Title of the Share')    
    ->addColumn('date_start', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
    ), 'Date of the start Share')
    ->addColumn('date_finish', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
    ), 'Date of the end Share') 
    ->addColumn('summary', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,    
    ), 'Summary')       
    ->addColumn('share_info', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ), 'Share Information')
    ->addColumn('image_prev', Varien_Db_Ddl_Table::TYPE_TEXT, 128, null, array(
        'nullable'  => false,    
    ), 'Preview Image')
    ->addColumn('status_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => '1',
    ), 'Status code')
    ->addColumn('products', Varien_Db_Ddl_Table::TYPE_TEXT, 128, null, array(
        'nullable'  => false,    
    ), 'Shared Products')
    ->setComment('Table of the promoactions and shares');
$installer->getConnection()->createTable($table);    
            
$installer->endSetup();