<?php
// Observer for the render Top Menu
class Space_Shares_Model_Observer
{
    // Adding Top Menu called 'Promo-Shares'
    public function addToTopmenu(Varien_Event_Observer $observer)
    {
        $menu = $observer->getMenu();
        $tree = $menu->getTree();
        
        // Create my native node
        $node = new Varien_Data_Tree_Node(array(
                'name'   => 'Promo-Shares',
                'id'     => 'shares',
                'url'    => Mage::getUrl('shares'),
        ), 'id', $tree, $menu);

        $menu->addChild($node);
    }
}