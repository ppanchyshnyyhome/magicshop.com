<?php
// Model for Shares Module and operations with uploading images 
class Space_Shares_Model_Shares extends Mage_Core_Model_Abstract
{
    // Constants for Statuses
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 2;
    
    // Const like Naming field image in form
    const FILE = 'image_prev';
    
    // Directory for Saving images
    public $imagePath = 'promotions';
    
    // Directory for resized images
    protected $_resizeImgDir = 'resized';


    /*
     * Initialization of the model in the registry
     */
     public function _construct()
    {
        $this->_init('shares/shares');
    }
    
    // Method for Saving Images before saving Model
    protected function _beforeSave()
    {
        if ($this->getData('image_prev/delete')) {
            $this->unsImage();
        }
        $path      = $this->getImagePath();
        $pathImage = $this->getData(self::FILE);
        
        if (!empty($_FILES[self::FILE]['name']) && empty($pathImage['delete'])) {
            try {
                $uploader = new Varien_File_Uploader(self::FILE);
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));

                // Set property for renaming uploaded files
                $uploader->setAllowRenameFiles(true);

                // Ser property for saving files with path like /i/m/image.jpg
                $uploader->setFilesDispersion(true);
                
                // Delete previous images before load new
                if (is_array($pathImage)) {
                    $delImg = $this->_prepareImageForDelete();
                    $delResizedImg = $this->_prepareResizedImageForDelete();                   
                    
                    unlink($delImg);
                    // Delete resized Image if exist
                    if (file_exists($delResizedImg)) {
                        unlink($delResizedImg);
                    }
                } 

                $this->setImage($uploader);
        
            } catch (Exception $e) {
                
            }
            // Action for simple saving Data without changing Image
        } elseif (!empty($pathImage['value']) && empty($pathImage['delete'])) {
            $fullPath = Mage::getBaseUrl('media') . $this->imagePath;
            $str = strlen($fullPath);
            $image = substr($pathImage['value'], $str);
            $this->setData(self::FILE, $image);
        }
        return parent::_beforeSave();
    }
    
    // Get full Image Path like media/promotions/ 
    public function getImagePath()
    {
        return Mage::getBaseDir('media') . DS . $this->imagePath . DS;
    }
    
    // Download and Move Uploaded Image File for Saving
    public function setImage($image)
    {
        if ($image instanceof Varien_File_Uploader) {
            $image->save($this->getImagePath());
            $image = $image->getUploadedFileName();
        }
        $this->setData(self::FILE, $image);
        return $this;
    }
    
    // Get image path for mini preview in edit form with full Url Path
    public function getImage()
    {
        if ($image = $this->getData(self::FILE)) {
            return Mage::getBaseUrl('media') . $this->imagePath . $image;
        } else {
            return '';
        }
    }
    
    // Prepares Image for deleting
    protected function _prepareImageForDelete()
    {
        $image = $this->getData(self::FILE);
        return str_replace(Mage::getBaseUrl('media'), Mage::getBaseDir('media') . DS, $image['value']);
    }
    
    // Prepares Resized Image for deleting
    protected function _prepareResizedImageForDelete() {
        $image = $this->getData(self::FILE);
  
        // Get image name
        $imageName = substr(strrchr($image['value'],"/"),1);
        return Mage::getBaseDir('media') . DS . $this->imagePath . DS . $this->_resizeImgDir . DS . $imageName;
    }
    
    // Delete and Unset Images
    public function unsImage()
    {
        $image = $this->getData(self::FILE);
        if (is_array($image)) {
            $image = $this->_prepareImageForDelete();
            $resizedImg = $this->_prepareResizedImageForDelete();
        } else {
            $image = $this->getImagePath() . $image;
        }
        
        if (file_exists($image)) {
            unlink($image);
            // Delete resized Image if exist
            unlink($resizedImg);
        }
        $this->setData(self::FILE, '');
        return $this;
    }
}