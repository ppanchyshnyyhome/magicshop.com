<?php
/**
 * Promo-Shares collection
 */
class Space_Shares_Model_Resource_Shares_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Shares table
     * @var string
     */
    protected $_sharesTable;

    /**
     * Define module Collection
     */
    protected function _construct()
    {
        $this->_init('shares/shares');
        $this->_sharesTable = $this->getTable('shares/shares_maintable');
    }
}