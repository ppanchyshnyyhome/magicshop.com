<?php
//Resource Model for Shares Module
class Space_Shares_Model_Resource_Shares extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('shares/shares_maintable', 'share_id');
    }
}