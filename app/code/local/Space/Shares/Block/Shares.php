<?php
// Block for rendering all Promo-Shares
class Space_Shares_Block_Shares extends Mage_Core_Block_Template
{
    // Shares Collection property
    protected $_sharesCollection;
    
    
    // Get the Shares Collection
    public function getSharesCollection()
    {
        if (!$this->__sharesCollection) {

            $sharesCollection = Mage::getModel('shares/shares')->getCollection();
            
            $sharesCollection->addFieldToFilter('status_id', 1)
                ->setOrder('share_id', 'DESC');

            $this->__sharesCollection = $sharesCollection;
        }

        return $this->__sharesCollection;
    }
    
    // Prepare layout and breadcrumbs
    public function _prepareLayout()
    {
	$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')
        ->addCrumb('home', array('label'=>Mage::helper('shares')
                ->__('Home'), 'title'=>Mage::helper('shares')
                ->__('Go to Home Page'), 'link'=>Mage::getBaseUrl()))
        ->addCrumb('shares', array('label'=>'Promo-Shares', 'title'=>'Promo-Shares'));
       

        return parent::_prepareLayout();
    }
        
    /**
     * Rendering Pager Block
     * @return Space_Shares_Block_Shares
     */
    protected function _beforeToHtml()
    {
        $pager = $this->getLayout()->createBlock('page/html_pager', 'shares.pager');
        
        $collection = $this->getSharesCollection();

        $config = Mage::getStoreConfig('shares/pagination/numbers');
        // Retrieve from the config number of pages to view
        $pager->setAvailableLimit(array(
            $config[0] => $config[0]
        ));
        
        $pager->setCollection($collection);
  
        $this->setChild('shares.pager', $pager);

        return $this;
    }
    
    // Get child block to pagering
    public function getPagerHtml()
    {
        return $this->getChildHtml('shares.pager');
    }
}
