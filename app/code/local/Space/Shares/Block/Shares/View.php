<?php
// Block for rendering and view current Promo-Share in frontend
class Space_Shares_Block_Shares_View extends Mage_Catalog_Block_Product_List
{
    // Shares Collection property
    protected $_currentSharesCollection;

    // Get the current Share Collection
    public function getCurrentSharesCollection()
    {
        if (!$this->_currentSharesCollection) {
            $id = (int) $this->getRequest()->getParam('share_id');

            $sharesCollection = Mage::getModel('shares/shares')->getCollection();
            
            //Filtering for view current Share
            $sharesCollection->addFieldToFilter('share_id', $id);

            $this->_currentSharesCollection = $sharesCollection;
        }

        return $this->_currentSharesCollection;
    }
    
    // Prepare layout and breadcrumbs
    public function _prepareLayout()
    {
	$breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')
        ->addCrumb('home', array('label'=>Mage::helper('shares')
                ->__('Home'), 'title'=>Mage::helper('shares')
                ->__('Go to Home Page'), 'link'=>Mage::getBaseUrl()))
        ->addCrumb('shares', array('label'=>Mage::helper('shares')
                ->__('Promo-Shares'), 'title'=>Mage::helper('shares')
                ->__('Go to List of Shares'), 'link'=>Mage::getBaseUrl() . 'shares'))
        ->addCrumb('shares_view', array('label'=>'Details', 'title'=>'Details'));
       

        return parent::_prepareLayout();
    }
}
