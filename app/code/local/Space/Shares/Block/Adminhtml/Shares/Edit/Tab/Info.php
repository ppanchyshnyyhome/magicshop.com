<?php
// Tab for Describe Share Info for 'Edit' and 'Add New' Forms
class Space_Shares_Block_Adminhtml_Shares_Edit_Tab_Info extends Mage_Adminhtml_Block_Widget_Form
{
    /**
    * Prepare form for edit and add new certain share
    */
    protected function _prepareForm()
    {   
        $helper = Mage::helper('shares');
        $share    = Mage::registry('current_share');
        
        $form   = new Varien_Data_Form();
        
        // Initialization of WYSIWYG configuration
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig(array(
            'add_variables' => true,
            'add_widgets' => true,
            'add_images' => false,
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
            'files_browser_window_width' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_width'),
            'files_browser_window_height'=> (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_height')
         ));
        
        $fieldset = $form->addFieldset('promoshares_form', array(
                'legend' => $helper->__('Share Information')
            ));

        $fieldset->addField('title', 'text', array(
            'label' => $helper->__('Title'),
            'required' => true,
            'name' => 'title'
        ));
        
        $fieldset->addField('image_prev', 'image', array(
            'label'    => $helper->__('Share Title Image'),
            'required' => false,
            'name'     => 'image_prev'
        ));
        
        $fieldset->addField('share_info', 'editor', array(
            'label' => $helper->__('Share Information'),
            'required' => true,
            'name'     => 'share_info',
            'config'   => $wysiwygConfig,
            'wysiwyg'  => true,
            'style'    => 'width:550px; height:300px'
        ));
        
        $fieldset->addField('summary', 'editor', array(
            'label'    => $helper->__('Summary'),
            'required' => false,
            'name'     => 'summary',
            'config'   => $wysiwygConfig,
            'wysiwyg'  => true,
            'style'    => 'width:550px; height:100px'
        ));
        
        $fieldset->addField('status_id', 'select', array(
            'label'     => $helper->__('Status'),
            'required'  => true,
            'name'      => 'status_id',
            'values'    => $helper->getShareStatusesOptionArray(),
        ));

        // Validate date format in form
        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
        
        $fieldset->addField('date_start', 'datetime', array(
            'format'   => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'label'    => $helper->__('Date Start'),
            'name'     => 'date_start',
            'required' => true,
            'format'   => $dateFormatIso,
            'class'    => 'required-entry validate-date validate-date-range date-range-start_date-from',
        ));
        
        $fieldset->addField('date_finish', 'datetime', array(
            'format'   => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'label'    => $helper->__('Date Expire'),
            'name'     => 'date_finish',
            'required' => true,
            'format'   => $dateFormatIso,
            'class'    => 'required-entry validate-date validate-date-range date-range-start_date-from',
        ));
        
        $data = $share->getData();
        $data['image_prev'] = $share->getImage();
        
        $form->setValues($data);

        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }
}