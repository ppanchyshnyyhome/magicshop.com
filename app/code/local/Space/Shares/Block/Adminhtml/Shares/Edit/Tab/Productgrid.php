<?php
// Tab for Picking products for Shares
class Space_Shares_Block_Adminhtml_Shares_Edit_Tab_Productgrid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     */
    public function __construct() 
    {
        parent::__construct();
        $this->setId('shares_products_grid');
        $this->setSaveParametersInSession(false);
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        
        if ((int) $this->getRequest()->getParam('id', 0)) {
            $this->setDefaultFilter(array('in_products' => 1));
        }
    }

    /**
     * Add filter for Collection
     *
     * @param object $column
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for 'in products' flag
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } else {
                if($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
    
    // Get Current Share Collection Selected Products
    protected function _getShare() {
        $id = (int) $this->getRequest()->getParam('id');
        
        $shareCollection = Mage::getModel('shares/shares')
                ->getCollection()
                ->addFieldToFilter('share_id', $id);
        
        return $shareCollection;
    }

    /**
     * Prepare collection
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product_link')
            ->getProductCollection()
            ->addAttributeToSelect('*');
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('in_products', array(
            'header_css_class'  => 'a-center',
            'type'              => 'checkbox',
            'name'              => 'in_products',
            'values'            => $this->_getSelectedProducts(),
            'align'             => 'center',
            'index'             => 'entity_id'
        ));

        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('catalog')->__('ID'),
            'sortable'  => true,
            'width'     => 60,
            'index'     => 'entity_id'
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('catalog')->__('Name'),
            'index'     => 'name'
        ));

        $this->addColumn('type', array(
            'header'    => Mage::helper('catalog')->__('Type'),
            'width'     => 100,
            'index'     => 'type_id',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));

        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();

        $this->addColumn('set_name', array(
            'header'    => Mage::helper('catalog')->__('Attrib. Set Name'),
            'width'     => 130,
            'index'     => 'attribute_set_id',
            'type'      => 'options',
            'options'   => $sets,
        ));

        $this->addColumn('status', array(
            'header'    => Mage::helper('catalog')->__('Status'),
            'width'     => 90,
            'index'     => 'status',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));

        $this->addColumn('visibility', array(
            'header'    => Mage::helper('catalog')->__('Visibility'),
            'width'     => 90,
            'index'     => 'visibility',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_visibility')->getOptionArray(),
        ));

        $this->addColumn('sku', array(
            'header'    => Mage::helper('catalog')->__('SKU'),
            'width'     => 80,
            'index'     => 'sku'
        ));

        $this->addColumn('price', array(
            'header'        => Mage::helper('catalog')->__('Price'),
            'type'          => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index'         => 'price'
        ));

        return parent::_prepareColumns();
    }

    /**
     * Retrieve grid URL
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getData('grid_url')
            ? $this->getData('grid_url')
            : $this->getUrl('*/*/productGrid', array('_current' => true));
    }
    
    /** 
     * Gets the selected products from the hidden input
     * @return array
     */ 
    protected function _getSelectedProducts()
    {
        $productsIds = $this->getSelectedProducts();
        // If $productsIds is NULL retrieve selected products from the DB
        if (!is_array($productsIds)) {
            $productsIds = $this->getSelectedSharedProducts();
        }
        return $productsIds;
    }
    
    /** 
     * Get Selected Product Ids from Collection of the Current Share
     * @return array
     */
    public function getSelectedSharedProducts() 
    {
        $products = array();
        $sharedProducts = $this->_getShare();
        // Retrive products for Current Share
        foreach($sharedProducts as $value){
            $products = explode(",", $value->getProducts());
        }
        return $products;
    }
}