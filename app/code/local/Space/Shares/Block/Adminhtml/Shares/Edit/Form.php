<?php
/*
 * Block which prepares forms 'Edit Promo-Share' and 'Add New Promo-Share' 
 */
class Space_Shares_Block_Adminhtml_Shares_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /*
     * Prepare layout for WYSIWYG 
     */
    protected function _prepareLayout() 
    {
        $return = parent::_prepareLayout();
        if(Mage::getSingleton('cms/wysiwyg_config')->isEnabled()){
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
        return $return;
    }
    
    /**
    * Prepare form for edit the certain share
    */
    protected function _prepareForm()
    {
        //Create form to edit and add the New Shares via Varien library
        $form = new Varien_Data_Form(array(
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save', array(
                        'id' => $this->getRequest()->getParam('id')
                    )),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ));
        
        // Declare Helper and Share Model
        $helper   = Mage::helper('shares');
        $share    = Mage::registry('current_share');
        
        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($share->getData());
        }
        
        $this->setForm($form);
        $form->setUseContainer(true);
        return parent::_prepareForm();
    }
}