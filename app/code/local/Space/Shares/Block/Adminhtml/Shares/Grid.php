<?php
/*
 * Grid of the all Promo-Shares
 */
class Space_Shares_Block_Adminhtml_Shares_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('sharesGrid');
        $this->setDefaultSort('share_id');
    }
    
    /*
     * Prepare collection of all Promo-Shares
     */    
    protected function _prepareCollection()
    {
        $model = Mage::getModel('shares/shares');
        $collection = $model->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /*
     * Prepare columns to display a list of Promo-Shares
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('shares');
        
        $this->addColumn('share_id', array(
            'header'        => $helper->__('ID'),
            'index'         => 'share_id',
            'width'         => '25px',
            'align'         => 'right',
        ));
        
        $this->addColumn('title', array(
            'header'        => $helper->__('Title'),
            'index'         => 'title',
            'align'         => 'center',
            'type'          => 'text',
            'weight'        => '600px'
        ));
    
        $this->addColumn('date_start', array(
            'header'        => $helper->__('Date Start'),
            'index'         => 'date_start',
            'type'          => 'date'
        ));
        
        $this->addColumn('date_finish', array(
            'header'        => $helper->__('Date Expire'),
            'index'         => 'date_finish',
            'type'          => 'date'
        ));
        
        if( !Mage::registry('usePendingFilter') ) {
            $this->addColumn('status', array(
                'header'        => $helper->__('Status'),
                'align'         => 'left',
                'type'          => 'options',
                'options'       => $helper->getShareStatuses(),
                'width'         => '100px',
                'index'         => 'status_id',
            ));
        }
        
        $this->addColumn('action', array(
            'header'    => Mage::helper('shares')->__('Action'),
            'width'     => 25,
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'shares/adminhtml_shares_renderer_action',
        ));
    }
    
    // Function for massDelete and massUpdate actions
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('share_id');
        $this->setMassactionIdFieldOnlyIndexValue(true);
        $this->getMassactionBlock()->setFormFieldName('shares');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('shares')->__('Delete'),
            'url'  => $this->getUrl(
                '*/*/massDelete',
                array('ret' => Mage::registry('usePendingFilter') ? 'pending' : 'index')
            ),
            'confirm' => Mage::helper('shares')->__('Are you sure?')
        ));

        $statuses = Mage::helper('shares')->getShareStatusesOptionArray();
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('update_status', array(
            'label'         => Mage::helper('shares')->__('Update Status'),
            'url'           => $this->getUrl(
                '*/*/massUpdateStatus',
                array('ret' => Mage::registry('usePendingFilter') ? 'pending' : 'index')
            ),
            'additional'    => array(
                'status'    => array(
                    'name'      => 'status',
                    'type'      => 'select',
                    'class'     => 'required-entry',
                    'label'     => Mage::helper('shares')->__('Status'),
                    'values'    => $statuses
                )
            )
        ));
    }
    
    /*
     * Get URL with the certain id for edit the certain Share
     */
    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
                    'id'        => $model->getShareId(),
                ));
    }
}