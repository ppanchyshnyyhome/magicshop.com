<?php
// Class for creating Tabs for 'Edit Share' Form and 'Add New Share' Form
class Space_Shares_Block_Adminhtml_Shares_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('shares_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('shares')->__('Promo-Share Info'));
    }
    
    // Create Tabs before Main Html
    protected function _beforeToHtml()
    {
        $this->addTab('share_info', array(
            'title'     => Mage::helper('shares')->__('Share Information'),
            'label'     => Mage::helper('shares')->__('Share Information'),
            'content'   => $this->getLayout()->createBlock('shares/adminhtml_shares_edit_tab_info')->toHtml(),
        ));

        $this->addTab('attached_products', array(
            'title'     => Mage::helper('shares')->__('Attached Products'),
            'label'     => Mage::helper('shares')->__('Attached Products'),
            'url'       => $this->getUrl('*/*/products', array('_current' => true)),
            'class'     => 'ajax',
        ));
        
        return parent::_beforeToHtml();
    }
}
