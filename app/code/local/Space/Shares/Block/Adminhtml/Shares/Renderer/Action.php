<?php
// Block which adds column for preview the Promo-Share
class Space_Shares_Block_Adminhtml_Shares_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{ 
    //Reference to Preview
    public function render(Varien_Object $row)
    {
        $href = Mage::getUrl('shares/index/view', array(
                '_current' => true,
                'share_id' => $row->getId()
            )); 
        
        return '<a href="' . $href . '" target="_blank">' . $this->__('Preview') . '</a>';
    }
}