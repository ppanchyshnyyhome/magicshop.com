<?php
/**
 * Block of the Grid Container of All Promo-Shares
 */
class Space_Shares_Block_Adminhtml_Shares extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected function _construct()
    {
        $helper = Mage::helper('shares');
        // _blockGroup is the folder where stores other blocks
        $this->_blockGroup = 'shares';
        $this->_controller = 'adminhtml_shares';

        $this->_headerText = $helper->__('Shares List');
        $this->_addButtonLabel = $helper->__('Add new Share');
    }
}