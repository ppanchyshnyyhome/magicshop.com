<?php 
class Develop_Citation_Adminhtml_CitationController extends Mage_Adminhtml_Controller_Action
{
    // Set actine menu and load layout
    protected function _initAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('catalog');
    }
    
    /**
     * Initialize product from request parameters
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initProduct()
    {
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $productId  = (int) $this->getRequest()->getParam('id');
        $product    = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if (!$productId) {
            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
        }

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
                Mage::logException($e);
            }
        }

        $attributes = $this->getRequest()->getParam('attributes');
        if ($attributes && $product->isConfigurable() &&
            (!$productId || !$product->getTypeInstance()->getUsedProductAttributeIds())) {
            $product->getTypeInstance()->setUsedProductAttributeIds(
                explode(",", base64_decode(urldecode($attributes)))
            );
        }

        // Required attributes of simple product for configurable creation
        if ($this->getRequest()->getParam('popup')
            && $requiredAttributes = $this->getRequest()->getParam('required')) {
            $requiredAttributes = explode(",", $requiredAttributes);
            foreach ($product->getAttributes() as $attribute) {
                if (in_array($attribute->getId(), $requiredAttributes)) {
                    $attribute->setIsRequired(1);
                }
            }
        }

        if ($this->getRequest()->getParam('popup')
            && $this->getRequest()->getParam('product')
            && !is_array($this->getRequest()->getParam('product'))
            && $this->getRequest()->getParam('id', false) === false) {

            $configProduct = Mage::getModel('catalog/product')
                ->setStoreId(0)
                ->load($this->getRequest()->getParam('product'))
                ->setTypeId($this->getRequest()->getParam('type'));

            /* @var $configProduct Mage_Catalog_Model_Product */
            $data = array();
            foreach ($configProduct->getTypeInstance()->getEditableAttributes() as $attribute) {

                /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                if(!$attribute->getIsUnique()
                    && $attribute->getFrontend()->getInputType()!='gallery'
                    && $attribute->getAttributeCode() != 'required_options'
                    && $attribute->getAttributeCode() != 'has_options'
                    && $attribute->getAttributeCode() != $configProduct->getIdFieldName()) {
                    $data[$attribute->getAttributeCode()] = $configProduct->getData($attribute->getAttributeCode());
                }
            }

            $product->addData($data)
                ->setWebsiteIds($configProduct->getWebsiteIds());
        }

        Mage::register('product', $product);
        Mage::register('current_product', $product);
        Mage::getSingleton('cms/wysiwyg_config')->setStoreId($this->getRequest()->getParam('store'));
        return $product;
    }
    
    // Create Block to display a list of citations in the backend
    public function indexAction()
    {   
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Citations'));
        
        if ($this->getRequest()->getParam('ajax')) {
            return $this->_forward('citationsGrid');
        }
        
        $this->_initAction();
        
        $contentBlock = $this->getLayout()->createBlock('citation/adminhtml_citations');
        $this->_addContent($contentBlock);
        
        
        $this->renderLayout();
    }
    
    /*
     * Select product and create a new citation
     */
    public function newAction()
    {        
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Citations'));
                
        $this->_title($this->__('New Citation'));

        $this->_initAction();
        
        // Create javascript form for citations
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        
        // Blocks for add new citation and grid products
        $this->_addContent($this->getLayout()->createBlock('citation/adminhtml_createcitation_add'));
        $this->_addContent($this->getLayout()->createBlock('citation/adminhtml_createcitation_product_grid'));

        $this->renderLayout();
    }
    
    /*
     * Action to edit citations
     */
    public function editAction()
    {
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Edit Citation'));
        
        $id = (int) $this->getRequest()->getParam('id');
        
        // Registration loaded id citations to be edited
        Mage::register('current_citation', Mage::getModel('citation/citation')->load($id));
        
        // Load layout and block for edit
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('citation/adminhtml_citation_edit'));
        $block = $this->getLayout()->getBlock('catalog.wysiwyg.js');
        $this->renderLayout();
    }
    
    /*
     * Action to save the edited  and the new citations
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('citation/citation');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));               
                if(!$model->getCreated()){
                    $model->setCreated(now());
                }
                
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Citation was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }
    
    /*
     * Action to delete some citations
     */
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('citation/citation')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Citation was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }
    
    /*
     * Product grid in the newAction
     */
    public function productGridAction()
    {
        $this->getResponse()->setBody($this->getLayout()->createBlock('citation/adminhtml_createcitation_product_grid')->toHtml());
    }
    /*
     * Citations grid in the indexAction
     */
    public function citationsGridAction()
    {
        $this->getResponse()->setBody($this->getLayout()->createBlock('citation/adminhtml_citation_grid')->toHtml());
    }
    
    /*
     * Action for json form to show product info in form to add a new citation
     */
    public function jsonProductInfoAction()
    {
        $response = new Varien_Object();
        $id = $this->getRequest()->getParam('id');
        if( intval($id) > 0 ) {
            $product = Mage::getModel('catalog/product')
                ->load($id);

            $response->setId($id);
            $response->addData($product->getData());
            $response->setError(0);
        } else {
            $response->setError(1);
            $response->setMessage(Mage::helper('catalog')->__('Unable to get the product ID.'));
        }
        $this->getResponse()->setBody($response->toJSON());
    }
    
    /*
     * Show citations grid in product edit form accord to certain product
     */
    public function showAction() 
    {
        //Initialization infromation about certain product
        $this->_initProduct();
        
        $this->getResponse()->setBody($this->getLayout()
                ->createBlock('citation/adminhtml_product_edit_tab_grid')
                //Get from registry current product ID (via _initProduct method)
                ->setProductId(Mage::registry('current_product')->getId())
                //Enable Ajax to filtering
                ->setUseAjax(true)
                ->toHtml());
    
    }
}
