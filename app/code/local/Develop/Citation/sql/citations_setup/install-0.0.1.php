<?php
/*
 * @var $installer Mage_Catalog_Model_Resource_Setup
 */
$installer = $this;
$tableCitations = $installer->getTable('citation/attached_products');

$installer->startSetup();
/*
 * Create table 'citation/attached_products'
 */
$installer->getConnection()->dropTable($tableCitations);
$table = $installer->getConnection($tableCitations)
    ->newTable($tableCitations)
    ->addColumn('citation_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Citation id')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Product ID')
    ->addColumn('citation', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
        ), 'Citation')
    ->addColumn('date_publication', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
    ), 'Date of publication')
    ->addColumn('journal', Varien_Db_Ddl_Table::TYPE_CHAR, null, array(
        'nullable'  => false,
    ), 'Name of the journal')
    ->addColumn('link', Varien_Db_Ddl_Table::TYPE_CHAR, null, array(
        'nullable'  => false,    
    ), 'Reference to the journal')
    ->addIndex($installer->getIdxName($tableCitations, array('product_id')),
        array('product_id'))
    ->addForeignKey($installer->getFkName($tableCitations, 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Table citation products');
$installer->getConnection()->createTable($table);

/**
 * Create table 'citation/citations_store' for binding quotes to store
 */
$installer->getConnection()->dropTable('citation/citations_store');
$table = $installer->getConnection()
    ->newTable($installer->getTable('citation/citations_store'))
    ->addColumn('citation_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'Citation ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')
    ->addIndex($installer->getIdxName('citation/citations_store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('citation/citations_store', 'citation_id', $tableCitations, 'citation_id'),
        'citation_id', $installer->getTable($tableCitations), 'citation_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('citation/citations_store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('Citations To Store Linkage Table');
$installer->getConnection()->createTable($table);

$installer->endSetup();