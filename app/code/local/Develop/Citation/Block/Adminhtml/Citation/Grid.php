<?php
/*
 * Grid of the all citations
 */
class Develop_Citation_Block_Adminhtml_Citation_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    
    public function __construct()
    {
        parent::__construct();
        $this->setId('citationsGrid');
        $this->setDefaultSort('date');
    }
    
    /*
     * Prepare collection of all citations
     */    
    protected function _prepareCollection()
    {
        $model = Mage::getModel('citation/citation');
        $collection = $model->getProductCollection();
        
        if ($this->getProductId() || $this->getRequest()->getParam('productId', false)) {
            $productId = $this->getProductId();
            if (!$productId) { 
                $productId = $this->getRequest()->getParam('productId');
            }
            $this->setProductId($productId);
            $collection->addEntityFilter($this->getProductId());
        }
        
        $collection->addStoreData();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    /*
     * Prepare columns to display a list of citations
     */
    protected function _prepareColumns()
    {
        $helper = Mage::helper('citation');
        
        $this->addColumn('citation_id', array(
            'header'        => $helper->__('ID'),
            'index'         => 'citation_id',
            'filter_index'  => 'ct.citation_id',
            'width'         => '25px',
            'align'         => 'right'
        ));
        
        $this->addColumn('name', array(
            'header'    => Mage::helper('citation')->__('Product Name'),
            'align'     =>'left',
            'type'      => 'text',
            'index'     => 'name',
            'escape'    => true,
            'width'     => '200px'
        ));
        
        $this->addColumn('product_id', array(
            'header'        => $helper->__('Product ID'),
            'index'         => 'product_id',
            'filter_index'  => 'ct.product_id',
            'width'         => '50px',
            'align'         => 'right'
        ));
        
        $this->addColumn('citation', array(
            'header'        => $helper->__('Citation'),
            'index'         => 'citation',
            'filter_index'  => 'ct.citation',
            'align'         => 'center',
            'type'          => 'text',
            'width'         => '400px'
        ));

        /**
         * Check is single store mode
         */        
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('visible_in', array(
                'header'        => Mage::helper('citation')->__('Visible In'),
                //display stores via index
                'index'         => 'stores',
                'type'          => 'store',
                'store_view'    => true,
                'store_all'     => true,
                'width'         => '125px'
            ));
        }
        
        $this->addColumn('date', array(
            'header'        => $helper->__('Date of the publication'),
            'index'         => 'date_publication',
            'filter_index'  => 'ct.date',
            'type'          => 'date'
        ));
        
        $this->addColumn('journal', array(
            'header'        => $helper->__('Name of the journal'),
            'index'         => 'journal',
            'filter_index'  => 'ct.journal',
            'type'          => 'text'
        ));
        
        $this->addColumn('link', array(
            'header'        => $helper->__('Reference to the journal'),
            'index'         => 'link',
            'filter_index'  => 'ct.link',
            'type'          => 'text'
        ));
    }
    
    /*
     * Get URL with the certain id for edit a certain citation
     */
    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
                    'id'        => $model->getCitationId(),
                    'productId' => $model->getProductId(),
                ));
    }
}