<?php
/*
 * Block of the edit citations form
 */
class Develop_Citation_Block_Adminhtml_Citation_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /*
     * Prepare layout for WYSIWYG 
     */
    protected function _prepareLayout() 
    {
        $return = parent::_prepareLayout();
        if(Mage::getSingleton('cms/wysiwyg_config')->isEnabled()){
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
        return $return;
    }
     /*
      * Prepare form for edit the certain citation
      */
    protected function _prepareForm()
    {
        
        $helper = Mage::helper('citation');
        $model = Mage::registry('current_citation');
        //Load product information via productId in Request
        $product = Mage::getModel('catalog/product')->load($model->getProductID());
        
        //Create form to edit the citations via Varien library
        $form = new Varien_Data_Form(array(
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save', array(
                        'id' => $this->getRequest()->getParam('id')
                    )),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ));
        
        $this->setForm($form);

        $fieldset = $form->addFieldset('citations_form', array(
            'legend' => $helper->__('New citation')
        ));
        
        $fieldset->addField('product_id', 'note', array(
            'label' => $helper->__('Product ID'),
            'required' => false,
            'name' => 'product_id',
            'text' => $model->getProductID()
        ));
        
        $fieldset->addField('product_name', 'note', array(
            'label'     => Mage::helper('review')->__('Product Name'),
            'required' => true,
            'text'      => '<a href="' . $this->getUrl('*/catalog_product/edit', array('id' => $product->getId())) . '" onclick="this.target=\'blank\'">' . $product->getName() . '</a>'
        ));
        
        $fieldset->addField('citation', 'editor', array(
            'label' => $helper->__('Citation'),
            'required' => true,
            'name' => 'citation',
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(
                array(
                'add_widgets'   => false,
                'add_variables' => false,
                'add_images'	=> false,
                'files_browser_window_url'=> $this->getBaseUrl().'admin/cms_wysiwyg_images/index/',
            )),
            'wysiwyg' => true,
        ));

        // Validate date format in form
        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
        
        $fieldset->addField('date_publication', 'date', array(
            'format'   => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'label'    => $helper->__('Date of the publication'),
            'name'     => 'date_publication',
            'required' => true,
            'format'   => $dateFormatIso,
            'class'    => 'required-entry validate-date validate-date-range date-range-start_date-from',
        ));
        
        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field =$fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'store_id',
                'label'     => Mage::helper('citation')->__('Visible In'),
                'title'     => Mage::helper('citation')->__('Visible In'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'store_id',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('journal', 'text', array(
            'label' => $helper->__('Name of the journal'),
            'required' => true,
            'name' => 'journal',
        ));
        
        $fieldset->addField('link', 'text', array(
            'label' => $helper->__('Reference to the journal'),
            'name' => 'link',
        ));
        
        $form->setUseContainer(true);

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }

        return parent::_prepareForm();
    }

}