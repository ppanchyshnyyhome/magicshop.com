<?php
/*
 * Block to display the edit form citations
 */
class Develop_Citation_Block_Adminhtml_Citation_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'citation';
        $this->_controller = 'adminhtml_citation';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('citation');
        /*
         * Pull model with the data of the selected id
         */
        $model  = Mage::registry('current_citation');

        if ($model->getId()) {
            return $helper->__("Edit Citation '%s'", $this->escapeHtml($model->getJournal()));
        } else {
            return $helper->__("Add New Citation");
        }
    }
}