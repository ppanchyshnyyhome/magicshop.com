<?php
/*
 * Block of the Grid Container 
 * Instance of class Develop_Citation_Block_Adminhtml_Citation_Grid
 */
class Develop_Citation_Block_Adminhtml_Citations extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected function _construct()
    {
        $helper = Mage::helper('citation');
        // _blockGroup is the folder where stores other blocks
        $this->_blockGroup = 'citation';
        $this->_controller = 'adminhtml_citation';

        $this->_headerText = $helper->__('Citations');
        $this->_addButtonLabel = $helper->__('Add new citation');
    }

}