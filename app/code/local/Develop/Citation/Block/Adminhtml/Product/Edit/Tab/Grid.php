<?php
/**
 * Citations products admin grid
 */
class Develop_Citation_Block_Adminhtml_Product_Edit_Tab_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    
    public function __construct()
    {
        parent::__construct();
        $this->setId('citationsGrid');
        $this->setDefaultSort('date');
    }
    
    /*
     * Prepare collection of certain product citations
     */
    protected function _prepareCollection()
    {
        $model = Mage::getModel('citation/citation');
        $collection = $model->getProductCollection();
   
        if ($this->getProductId() || $this->getRequest()->getParam('productId', false)) {
            $productId = $this->getProductId();
            if (!$productId) { 
                $productId = $this->getRequest()->getParam('productId');
            }
            $this->setProductId($productId);
            
            $collection->addEntityFilter($this->getProductId());
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {

        $helper = Mage::helper('citation');
        
        $this->addColumn('citation_id', array(
            'header'        => Mage::helper('citation')->__('ID'),
            'align'         => 'right',
            'width'         => '50px',
            'index'         => 'citation_id',
        ));
        
        $this->addColumn('name', array(
                'header'=> Mage::helper('citation')->__('Product'),
                'index' => 'name',
        ));
        
        $this->addColumn('journal', array(
            'header' => $helper->__('Name of the journal'),
            'index' => 'journal',
            'type' => 'text'
        ));
        
        $this->addColumn('date', array(
            'header' => $helper->__('Date of the publication'),
            'index' => 'date_publication',
            'type' => 'date'
        ));
        
        $this->addColumn('citation', array(
            'header' => $helper->__('Citation'),
            'index'  => 'citation',
            'type'   => 'text',
            'align'  => 'center',
        ));
        
        $this->addColumn('link', array(
            'header' => $helper->__('Reference to the journal'),
            'index' => 'link',
            'type' => 'text'
        ));
        
        $this->addColumn('action',
            array(
                'header'    => Mage::helper('adminhtml')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'     => 'getCitationId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('adminhtml')->__('Edit'),
                        'url'     => array(
                            'base'=>'*/citation/edit',
                            'params'=> array(
                                'productId' => $this->getProductId(),
                            )
                         ),
                        'target'=>'_blank',
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false
        ));
    }
   
    /**
     * Hide grid mass action elements
     *
     * @return Develop_Citation_Block_Adminhtml_Product_Edit_Citations_Grid
     */
    protected function _prepareMassaction()
    {
        return $this;
    }
    
}