<?php
/*
 * Block of the form to add a new citation
 */
class Develop_Citation_Block_Adminhtml_Createcitation_Add_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /*
     * Prepare layout for WYSIWYG 
     */
    protected function _prepareLayout() 
    {
        $return = parent::_prepareLayout();
        if(Mage::getSingleton('cms/wysiwyg_config')->isEnabled()){
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        }
        return $return;
    }
    
    /*
     * Prepare form for add a new citation
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
                'add_citation_form', 
                array('legend' => Mage::helper('citation')
                ->__('Citation Details'))
        );

        $fieldset->addField('product_name', 'note', array(
            'label'     => Mage::helper('citation')->__('Product'),
            'text'      => 'product_name',
        ));
        
        //Add citation field with WYSIWYG Editor
        $fieldset->addField('citation', 'editor', array(
            'name'      => 'citation',
            'title'     => Mage::helper('citation')->__('Citation'),
            'label'     => Mage::helper('citation')->__('Citation'),
            'style'     => 'height: 200px;',
            'required'  => true,
            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(
                array(
                'add_widgets'   => false,
                'add_variables' => false,
                'add_images'	=> false,
                'files_browser_window_url'=> $this->getBaseUrl().'admin/cms_wysiwyg_images/index/',
            )),
            'wysiwyg' => true,
        ));
        
        // Validate date format in form
        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
        
        $fieldset->addField('date_publication', 'date', array(
            'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'label'     => Mage::helper('citation')->__('Date of publication'),
            'name' => 'date_publication',
            'required'  => true,
            'format'    => $dateFormatIso,
            'class'    => 'required-entry validate-date validate-date-range date-range-start_date-from',
        ));
        
        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field =$fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('citation')->__('Visible In'),
                'title'     => Mage::helper('citation')->__('Visible In'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }
        
        $fieldset->addField('journal', 'text', array(
            'name'      => 'journal',
            'title'     => Mage::helper('citation')->__('Name of the journal'),
            'label'     => Mage::helper('citation')->__('Name of the journal'),
            'maxlength' => '50',
            'required'  => true,
        ));

        $fieldset->addField('link', 'text', array(
            'name'      => 'link',
            'title'     => Mage::helper('citation')->__('Reference to the journal'),
            'label'     => Mage::helper('citation')->__('Reference to the journal'),
            'maxlength' => '255',
            'required'  => false,
        ));

        $fieldset->addField('product_id', 'hidden', array(
            'name'      => 'product_id',
        ));

        
        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));

        $this->setForm($form);
    }
    
}