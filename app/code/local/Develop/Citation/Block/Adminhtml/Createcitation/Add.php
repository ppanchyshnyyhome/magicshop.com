<?php
/*
 * Prepare javascript gridRowClick method in the grid products for add a new citation
*/
class Develop_Citation_Block_Adminhtml_Createcitation_Add extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_blockGroup = 'citation';
        $this->_controller = 'adminhtml_createcitation';
        $this->_mode = 'add';

        $this->_updateButton('save', 'label', Mage::helper('citation')->__('Save Citation'));
        $this->_updateButton('save', 'id', 'save_button');
        $this->_updateButton('reset', 'id', 'reset_button');

        $this->_formScripts[] = '
            toggleParentVis("add_citation_form");
            toggleVis("save_button");
            toggleVis("reset_button");
        ';

        // js-method for show and hide 'add new citation form' when clicking on the product
        $this->_formInitScripts[] = '
            //<![CDATA[
            var citation = function() {
                return {
                    productInfoUrl : null,
                    formHidden : true,

                    gridRowClick : function(data, click) {
                        if(Event.findElement(click,\'TR\').title){
                                 citation.productInfoUrl = Event.findElement(click,\'TR\').title;
                            citation.loadProductData();
                            citation.showForm();
                            citation.formHidden = false;
                        }
                    },

                    loadProductData : function() {
                        var con = new Ext.lib.Ajax.request(\'POST\', citation.productInfoUrl, {success:citation.reqSuccess,failure:citation.reqFailure}, {form_key:FORM_KEY});
                    },

                    showForm : function() {
                        toggleParentVis("add_citation_form");
                        toggleVis("productGrid");
                        toggleVis("save_button");
                        toggleVis("reset_button");
                    },
                    
                    reqSuccess :function(o) {
                        var response = Ext.util.JSON.decode(o.responseText);
                        if( response.error ) {
                            alert(response.message);
                        } else if( response.id ){
                            $("product_id").value = response.id;

                            $("product_name").innerHTML = \'<a href="' . $this->getUrl('*/catalog_product/edit') . 'id/\' + response.id + \'" target="_blank">\' + response.name + \'</a>\';
                        } else if( response.message ) {
                            alert(response.message);
                        }
                    }
                }
            }();
           //]]>
        ';
    }

    public function getHeaderText()
    {
        return Mage::helper('citation')->__('Add New Citation');
    }
}