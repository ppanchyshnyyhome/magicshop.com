<?php
/* 
 * Detailed Product Citations
 */
class Develop_Citation_Block_Product_View_List extends Develop_Citation_Block_Product_View
{

    //Get id of certaion product
    public function getProductId()
    {
        return Mage::registry('product')->getId();
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        return $this;
    }
}

