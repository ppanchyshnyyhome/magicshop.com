<?php
/*
 * Citation Collection of the current product in product view form
 */
class Develop_Citation_Block_Product_View extends Mage_Catalog_Block_Product_View
{
    protected $_citationsCollection;

    //Create citations collection of certain product
    public function getCitationsCollection()
    {
        if (null === $this->_citationsCollection) {
            $this->_citationsCollection = Mage::getModel('citation/citation')->getProductCollection()
                ->addStoreFilter(Mage::app()->getStore()->getId())
                ->addEntityFilter($this->getProduct()->getId());
        }
        return $this->_citationsCollection;
    }
}