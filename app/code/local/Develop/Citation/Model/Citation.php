<?php
class Develop_Citation_Model_Citation extends Mage_Core_Model_Abstract
{
    /*
     * Initialization of the model in the registry
     */
     public function _construct()
    {
        $this->_init('citation/citation');
        $this->_citationStoreTable    = $this->getTable('citation/citations_store');
    }
    
    /*
     * Get product collection
     */
    public function getProductCollection()
    {
        return Mage::getResourceModel('citation/citation_product_collection');
    }
    
}