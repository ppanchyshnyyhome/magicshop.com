<?php
/*
 * Observer for add Citation Tab to Product Edit Form
 */
class Develop_Citation_Model_Observer
{
    const PRODUCT_TABS_BLOCK_NAME = 'product_tabs';
    
    public function addCitationTabToProductEdit($observer)
    {
        $block = $observer->getBlock();
        
        //Check the other blocks to find our block
        if ($block->getNameInLayout() != self::PRODUCT_TABS_BLOCK_NAME) {
            return;
        }
        
        //Add my tab after 'categories' tab in edit product form
        if( $block->getRequest()->getParam('id', false) ) {
            if (Mage::helper('catalog')->isModuleEnabled('Develop_Citation')) {
                $block->addTabAfter('citations', array(
                                    'label' => Mage::helper('catalog')->__('Citations'),
                                    'url'   => Mage::getUrl('*/citation/show', array('_current' => true)),
                                    'class' => 'ajax',
                     ), 'categories');
            }
        
        }
    }
}
