<?php
/**
 * Citations collection resource model
 *
 */
class Develop_Citation_Model_Resource_Citation_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Citation table
     *
     * @var string
     */
    protected $_citationTable;

    /**
     * Citation store table
     *
     * @var string
     */
    protected $_citationStoreTable;

    /**
     * Add store data flag
     * @var bool
     */
    protected $_addStoreDataFlag   = false;

    /**
     * Define module
     *
     */
    protected function _construct()
    {
        $this->_init('citation/citation');
        $this->_citationTable         = $this->getTable('citation/attached_products');
        $this->_citationStoreTable    = $this->getTable('citation/citations_store');

    }

    /**
     * Add store filter
     *
     * @param int|array $storeId
     * @return Develop_Citation_Model_Resource_Citation_Collection
     */
    public function addStoreFilter($storeId)
    {
        $inCond = $this->getConnection()->prepareSqlCondition('store.store_id', array('in' => $storeId));
        $this->getSelect()->join(array('store'=>$this->_citationStoreTable),
            'main_table.citation_id = store.citation_id',
            array());
        $this->getSelect()->where($inCond);
        return $this;
    }

    /**
     * Add stores data
     *
     * @return Develop_Citation_Model_Resource_Citation_Collection
     */
    public function addStoreData()
    {
        $this->_addStoreDataFlag = true;
        return $this;
    }
    
    /**
     * Set date order
     *
     * @param string $dir
     * @return Develop_Citation_Model_Resource_Citation_Collection
     */
    public function setDateOrder($dir = 'DESC')
    {
        $this->setOrder('main_table.date_publication', $dir);
        return $this;
    }

    /**
     * Add store data
     *
     */
    protected function _addStoreData()
    {
        $adapter = $this->getConnection();

        $citationsIds = $this->getColumnValues('citation_id');
        $storesTocitations = array();
        if (count($citationsIds)>0) {
            $citationIdCondition = $this->_getConditionSql('citation_id', array('in' => $citationsIds));
            $storeIdCondition = $this->_getConditionSql('store_id', array('gt' => 0));
            $select = $adapter->select()
                ->from($this->_citationStoreTable)
                ->where($citationIdCondition)
                ->where($storeIdCondition);
            $result = $adapter->fetchAll($select);
            foreach ($result as $row) {
                if (!isset($storesToCitations[$row['citation_id']])) {
                    $storesToCitations[$row['citation_id']] = array();
                }
                $storesToCitations[$row['citation_id']][] = $row['store_id'];
            }
        }

        foreach ($this as $item) {
            if (isset($storesToCitations[$item->getCitationId()])) {
                $item->setData('stores', $storesToCitations[$item->getCitationId()]);
            } else {
                $item->setData('stores', array());
            }

        }
    }
}
