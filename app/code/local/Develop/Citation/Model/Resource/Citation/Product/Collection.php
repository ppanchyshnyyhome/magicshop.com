<?php
/**
 * Citation Product Collection
 */
class Develop_Citation_Model_Resource_Citation_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    /**
     * Entities alias
     *
     * @var array
     */
    protected $_entitiesAlias        = array();

    /**
     * Filter by stores for the collection
     *
     * @var array
     */
    protected $_storesIds           = array();
    
    /**
     * Citation store table
     *
     * @var string
     */
    protected $_citationStoreTable;
    
    /**
     * Add store data flag
     *
     * @var boolean
     */
    protected $_addStoreDataFlag     = false;
    
    /**
     * Use analytic function flag
     * If true - allows to prepare final select with analytic functions
     *
     * @var bool
     */
    protected $_useAnalyticFunction         = false;
    
    
    protected function _construct()
    {
        $this->_init('catalog/product');
        $this->setRowIdFieldName('citation_id');
        $this->_citationStoreTable = Mage::getSingleton('core/resource')->getTableName('citation/citations_store');
        $this->_initTables();
    }

    /**
     * Init select
     *
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        $this->_joinFields();
        return $this;
    }

    /**
     * Add entity filter
     *
     * @param int $entityId
     */
    public function addEntityFilter($entityId)
    {
        $this->getSelect()
            ->where('ct.product_id = ?', $entityId);
        return $this;
    }

    /**
     * Join fields to entity
     *
     */
    protected function _joinFields()
    {
        $citationTable = Mage::getSingleton('core/resource')->getTableName('citation/attached_products');

        $this->addAttributeToSelect('name');

        $this->getSelect()
            ->join(array('ct' => $citationTable),
                'ct.product_id = e.entity_id',
                array('ct.*'));
        return $this;
    }
    
    /**
     * Retrive all ids for collection
     *
     * @param unknown_type $limit
     * @param unknown_type $offset
     * @return array
     */
    public function getAllIds($limit = null, $offset = null)
    {
        $idsSelect = clone $this->getSelect();
        $idsSelect->reset(Zend_Db_Select::ORDER);
        $idsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $idsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $idsSelect->reset(Zend_Db_Select::COLUMNS);
        $idsSelect->columns('ct.citation_id');
        return $this->getConnection()->fetchCol($idsSelect);
    }
    
    /**
     * Set date order
     *
     * @param string $dir
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    public function setDateOrder($dir = 'ASC')
    {
        $this->setOrder('ct.date', $dir);
        return $this;
    }
    
    /**
     * Render SQL for retrieve product count
     *
     * @return string
     */
    public function getSelectCountSql()
    {
        $select = parent::getSelectCountSql();
        $select->reset(Zend_Db_Select::COLUMNS)
            ->columns('COUNT(e.entity_id)')
            ->reset(Zend_Db_Select::HAVING);

        return $select;
    }
    
    /**
     * Add stores data
     *
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    public function addStoreData()
    {
        $this->_addStoreDataFlag = true;
        return $this;
    }
    
    /**
     * Adds store filter into array
     *
     * @param mixed $storeId
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    public function addStoreFilter($storeId = null)
    { 
        if (is_null($storeId)) {
            $storeId = $this->getStoreId();
        }

        parent::addStoreFilter($storeId);

        if (!is_array($storeId)) {
            $storeId = array($storeId);
        }

        if (!empty($this->_storesIds)) {
            $this->_storesIds = array_intersect($this->_storesIds, $storeId);
        } else {
            $this->_storesIds = $storeId;
        }
        return $this;
    }

    /**
     * Adds specific store id into array
     *
     * @param array $storeId
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    public function setStoreFilter($storeId)
    {
        if (is_array($storeId) && isset($storeId['eq'])) {
            $storeId = array_shift($storeId);
            
        }

        if (!is_array($storeId)){
            $storeId = array($storeId);
        }

        if (!empty($this->_storesIds)){
            $this->_storesIds = array_intersect($this->_storesIds, $storeId);
        } else {
            $this->_storesIds = $storeId;
        }
        
        return $this;
    }

    /**
     * Applies all store filters in one place to prevent multiple joins in select
     *
     * @param null|Zend_Db_Select $select
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    protected function _applyStoresFilterToSelect(Zend_Db_Select $select = null)
    {
        $adapter = $this->getConnection();
        $storesIds = $this->_storesIds;

        if (is_null($select)){
            $select = $this->getSelect();
        }

        if (is_array($storesIds) && (count($storesIds) == 1)) {
            $storesIds = array_shift($storesIds);
        }

        if (is_array($storesIds) && !empty($storesIds)) {
            $inCond = $adapter->prepareSqlCondition('store.store_id', array('in' => $storesIds));
            $select->join(array('store' => $this->_citationStoreTable),
                'ct.citation_id = store.citation_id AND ' . $inCond,
                array())
            ->group('ct.citation_id');

            $this->_useAnalyticFunction = true;
        } else {
            $select->join(array('store' => $this->_citationStoreTable),
                $adapter->quoteInto('ct.citation_id=store.citation_id AND store.store_id = ?', (int)$storesIds),
                array());
        }
        return $this;
    }
    
    /**
     * Redeclare parent method for store filters applying
     *
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    protected function _beforeLoad()
    {
        parent::_beforeLoad();
        $this->_applyStoresFilterToSelect();

        return $this;
    }
    
    /**
     * Action after load
     *
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    protected function _afterLoad()
    {
        parent::_afterLoad();
        if ($this->_addStoreDataFlag) {
            $this->_addStoreData();
        }
        return $this;
    }
    
    /**
     * Retrieves column values
     *
     * @param string $colName
     * @return array
     */
    public function getColumnValues($colName)
    {
        $col = array();
        foreach ($this->getItems() as $item) {
            $col[] = $item->getData($colName);
        }
        return $col;
    }
    
    /**
     * Add store data
     *
     */
    protected function _addStoreData()
    {
        $adapter = $this->getConnection();

        $citationsIds = $this->getColumnValues('citation_id');
        $storesToCitations = array();
        if (count($citationsIds) > 0) {
            $citationIdCondition = $this->_getConditionSql('citation_id', array('in' => $citationsIds));
            $storeIdCondition = $this->_getConditionSql('store_id', array('gt' => 0));
            $select = $adapter->select()
                    //from "citations_store"
                ->from($this->_citationStoreTable)
                    // "where citation_id IN('1', '2', '3', '4', '5')" 
                ->where($citationIdCondition)
                    //where store_id > 0
                ->where($storeIdCondition);
            $result = $adapter->fetchAll($select);
            
            foreach ($result as $row) {
                if (!isset($storesToCitations[$row['citation_id']])) {
                    $storesToCitations[$row['citation_id']] = array();
                }
                $storesToCitations[$row['citation_id']][] = $row['store_id'];
            }
        }

        foreach ($this as $item) {
            if (isset($storesToCitations[$item->getCitationId()])) {
                $item->setData('stores', $storesToCitations[$item->getCitationId()]);
            } else {
                $item->setData('stores', array());
            }

        }
    }
    
     /**
     * Add attribute to filter
     *
     * @param Mage_Eav_Model_Entity_Attribute_Abstract|string $attribute
     * @param array $condition
     * @param string $joinType
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    public function addAttributeToFilter($attribute, $condition = null, $joinType = 'inner')
    {
        switch($attribute) {
            case 'ct.citation_id':
            case 'ct.product_id':
            case 'ct.citation':
            case 'ct.date':
            case 'ct.journal':
            case 'ct.link':    
            case 'stores':
                $this->setStoreFilter($condition);
                break;
            default:
                parent::addAttributeToFilter($attribute, $condition, $joinType);
                break;
        }
        return $this;
    }
    
    /**
     * Set order to attribute
     *
     * @param string $attribute
     * @param string $dir
     * @return Develop_Citation_Model_Resource_Citation_Product_Collection
     */
    public function setOrder($attribute, $dir = 'DESC')
    {
        switch($attribute) {
            case 'ct.citation_id':
            case 'ct.product_id':
            case 'ct.citation':
            case 'ct.date':
            case 'ct.journal':
            case 'ct.link':
            case 'stores':
                // No way to sort
                break;
            default:
                parent::setOrder($attribute, $dir);
        }
        return $this;
    }
}
