<?php
/**
 * Observer to add custom download attribute type
 */

class Develop_Download_Model_Observer
{
    /**
     * Add custom Downloadable Attribute to options on "Add Attribute" form
     */
    public function addAttributeDownloadType(Varien_Event_Observer $observer)
    {
         // adminhtml_product_attribute_types

        $response = $observer->getEvent()->getResponse();
        $types = $response->getTypes();
        
        $types[] = array(
            'value' => 'downloadable',
            'label' => Mage::helper('download')->__('Download Files'),
            'hide_fields' => array(
                'is_unique',
                'is_required',
                'frontend_class',
                'is_configurable',

                '_scope',
                '_default_value',
                '_front_fieldset',
            ));
        
        $response->setTypes($types);

        return $this;
    }
    
    /**
     * Add custom field to the form
     */
    public function addAttributeDownloadTypeFormField(Varien_Event_Observer $observer)
    {
        $form = $observer->getForm();
        
        // Assigns form where are rendered custom field
        $fieldset = $form->getElement('base_fieldset');

        $fieldset->addField('download_path', 'text', array(
            'name'     => 'download_path',
            'label'    => Mage::helper('download')->__('Download Path'),
            'title'    => Mage::helper('download')->__('Download Path'),
            'note'     => Mage::helper('download')->__('Path where are stored selected files'),
        ));
    }
    
    /**
     * Add custom element type for attributes form
     *
     * @param   Varien_Event_Observer $observer
     */
    public function updateElementTypes(Varien_Event_Observer $observer)
    {
        $response = $observer->getEvent()->getResponse();
        $types = $response->getTypes();
        $types['downloadable'] = Mage::getConfig()->getBlockClassName('download/element_downloadable');
        $response->setTypes($types);
        return $this;
    }
}