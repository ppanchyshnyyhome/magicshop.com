<?php
/* 
 * Helper for Develop Loader module
 */

class Develop_Loader_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get Share statuses with their codes
     *
     * @return array
     */
    public function getTypes()
    {
        return array(
            Develop_Loader_Model_Loaded::DIRECTORY_TYPE    => $this->__('Directory'),
            Develop_Loader_Model_Loaded::FILE_TYPE         => $this->__('File'),
        );
    }
    
}