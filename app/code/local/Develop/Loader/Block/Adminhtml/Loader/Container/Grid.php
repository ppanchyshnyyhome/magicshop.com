<?php
/**
 * Grid which describes all loaded files
 * @package Develop_Loader
 */

class Develop_Loader_Block_Adminhtml_Loader_Container_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Initialize Grid block
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->_defaultLimit = 20;
        $this->setId('loader_grid');
        $this->setUseAjax(true);
    }

    /**
     * Creates collection of loaded files if it has not been created yet
     *
     * @return Develop_Loader_Model_Loaded_Collection
     */
    public function getCollection()
    {
        if (!$this->_collection) {
            $this->_collection = Mage::getModel('loader/loaded_collection');
        }
        return $this->_collection;
    }

    /**
     * Prepare Loaded File Collection for Grid
     *
     * @return Develop_Loader_Block_Adminhtml_Loaded_Collection
     */
    protected function _prepareCollection()
    {
        $collection = $this->getCollection()
                ->setOrder('name', 'asc');
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns
     *
     */
    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'  => Mage::helper('loader')->__('Name'),
            'index'   => 'name',
        ));
        
        $this->addColumn('type', array(
            'header'  => Mage::helper('loader')->__('Type'),
            'index'   => 'type',
            'type'    => 'options',
            'options' => array(
                    Develop_Loader_Model_Loaded::DIRECTORY_TYPE => Mage::helper('loader')->__('Directory'),
                    Develop_Loader_Model_Loaded::FILE_TYPE 	=> Mage::helper('loader')->__('File')
                ),
        ));
        
        $this->addColumn('size', array(
            'header'  => Mage::helper('loader')->__('Size, bytes'),
            'index'   => 'size',
        ));
        
        $this->addColumn('modified_at',
            array(
                'header'    => Mage::helper('loader')->__('Date Updated'),
                'index'     => 'modified_at',
                'gmtoffset' => true,
                'type'      => 'datetime'
        ));

        $this->addColumn('image', array(
            'header'   => Mage::helper('loader')->__('Image'),
            'align'    => 'left',
            'index'    => 'image',
            'width'    => '110',
            'renderer' => 'Develop_Loader_Block_Adminhtml_Grid_Renderer_Image',
        ));
        
        return parent::_prepareColumns();
    }

    /**
     * Self URL getter
     *
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->getUrl('*/*/grid');
    }

    /**
     * Row URL getter
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this;
    }
    
}