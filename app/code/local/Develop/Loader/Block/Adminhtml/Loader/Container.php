<?php
/**
* Grid container for Develop Loader Module
 */
class Develop_Loader_Block_Adminhtml_Loader_Container extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $helper = Mage::helper('loader');
        // _blockGroup is the folder where stores other blocks, it is Module Name
        $this->_blockGroup = 'loader';
        // _controller - it is folder which defines where stores our grid file, in this case - Adminhtml/Loader/Container
        $this->_controller = 'adminhtml_loader_container';
        $this->_headerText = $helper->__('Loaded Files');
        
        parent::__construct();
        // Remove 'Add New' button
        $this->_removeButton('add');
    }
}