<?php
/**
 * Render Image in Loaded files Grid
 * @package Develop_Loader
 */
class Develop_Loader_Block_Adminhtml_Grid_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{   
    // Rendering of images
    public function render(Varien_Object $row)
    {
        return $this->_getValue($row);
    }
    
    // Gets value-url of images if that exists
    protected function _getValue(Varien_Object $row)
    {           
        $val = $row['filename'];
        $newDir = str_replace(Mage::getBaseDir(), Mage::getBaseUrl(), $val);
        $newDir = str_replace('index.php' . DS . DS, '', $newDir);
        //$url = $newDir;
        $url = 'http://magicshop.com/media/Promo/spec.jpg';
        if(ereg('.(jpg|jpeg|png|gif)', $url)){
            $out = "<a href='#' onclick='popWin(".$url.",'windowName','width=800,height=600,resizable=yes,scrollbars=yes')><img src=" . $url . " width='100px;' height ='100px;' /><a>";
        } else{
            $out = '';
        }

        return $out;
    }
    
    
}

