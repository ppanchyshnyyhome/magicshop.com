<?php
/* 
 * Controller for Develop Loader Module
 */

class Develop_Loader_Adminhtml_LoaderController extends Mage_Adminhtml_Controller_Action
{
    // Set actine menu and load layout
    protected function _initAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('loader');
    }
    
    // Action for showing grid of files
    public function indexAction()
    {
        $this->_title($this->__('Loaded Files'));
        $this->_initAction();
        $this->renderLayout();
    }
    
    // Action for sorting
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock("loader/adminhtml_loader_container_grid")->toHtml()
        );
    }
}