<?php
/**
 * Develop Loader model
 *
 * @category    Develop
 * @package     Develop_Loader
 */
class Develop_Loader_Model_Loaded extends Varien_Object
{
    /**
     * 
     */
    const FILE_TYPE      = 1;
    const DIRECTORY_TYPE = 2;
}
