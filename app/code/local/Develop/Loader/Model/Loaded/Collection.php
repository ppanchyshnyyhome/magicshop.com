<?php
/**
 * Files collection of Develop Loader module
 *
 * @package     Develop_Loader
 */
class Develop_Loader_Model_Loaded_Collection extends Varien_Data_Collection_Filesystem
{
    /** 
     * Files and folders regexsp
     *
     * @var string
     */
    protected $_allowedDirsMask     = '/^[a-z0-9\.\-]+$/i';
    protected $_allowedFilesMask    = '/^[a-z0-9\.\-\_]+$/i';
    

    /**
     * Base dir where files and directories are located
     *
     * @var string
     */
    protected $_baseDir = '';
    
    /**
     * Type of the collection
     *
     * @var string
     */
    protected $_collType = '';

    /**
     * Set or create (if not exists) base dir
     */
    public function __construct()
    {
        $this->_baseDir = Mage::getBaseDir() . DS . Mage::getStoreConfig('general/settings/directory');
        $this->_collType = Mage::getStoreConfig('general/settings/collect_type');

        $io = new Varien_Io_File();
        $io->setAllowCreateFolders(true)->createDestinationDir($this->_baseDir);
        
        if($this->_collType == 1){
            $this->setCollectRecursively(true);
        }else{
            $this->setCollectRecursively(false);
        }
        $this->addTargetDir($this->_baseDir);
        $this->setCollectDirs(true);
    }

    /**
     * Row generator
     *
     * @param string $filename
     * @return array
     */
    protected function _generateRow($filename)
    {
        $row = parent::_generateRow($filename);
        $row['name'] = preg_replace('/\.$/', '', str_replace($this->_baseDir . DS, '', $filename));

        $row['modified_at'] = date("F d Y H:i:s.", filemtime($filename));
        // Regular expression for checking File type
        $regExp = (preg_replace("/.*?\./", '', $filename));
        
        // Check: it is File or Directory? And do some operations
//        if(!strchr($regExp, Mage::getBaseDir())){
//            $row['type'] = Develop_Loader_Model_Loaded::FILE_TYPE;
//            $row['size'] = $row['size'] = filesize($filename);
//            $row['name'] = $row['basename'];
//        }else{
//            $row['type'] = Develop_Loader_Model_Loaded::DIRECTORY_TYPE;
//            if($this->folderSize($filename) != 0){
//                $row['size'] = $this->folderSize($filename);
//            }else {
//                $row['size'] = 'Dir is empty';
//            } 
//        }
        
        if(is_file($filename)){
            $row['type'] = Develop_Loader_Model_Loaded::FILE_TYPE;
            $row['size'] = $row['size'] = filesize($filename);
            $row['name'] = $row['basename'];
        }else{
            $row['type'] = Develop_Loader_Model_Loaded::DIRECTORY_TYPE;
            if($this->folderSize($filename) != 0){
                $row['size'] = $this->folderSize($filename);
            }else {
                $row['size'] = 'Dir is empty';
            } 
        }
        $row['id'] = $filename;
        
        array_pop($folder);
        
        return $row;
    }
    
    /** Get Folder size
     * 
     * @param string $dir
     * @return type
     */
    public function folderSize($dir){
        $count_size = 0;
        $count = 0;
        $dir_array = scandir($dir);
        foreach($dir_array as $key=>$filename){
            if($filename!=".." && $filename!="."){
               if(is_dir($dir."/".$filename)){
                    $new_foldersize = $this->folderSize($dir."/".$filename);
                    $count_size = $count_size+ $new_foldersize;
                }else if(is_file($dir."/".$filename)){
                  $count_size = $count_size + filesize($dir."/".$filename);
                  $count++;
                }
            }   
        }
        return $count_size;
    }
    
    // Redefines Filter Callback
    public function filterCallbackLike($field, $filterValue, $row)
    {
        $filterValueRegex = str_replace('%', '(.*?)', preg_quote($filterValue, '/'));
        
        return (bool)preg_match($filterValueRegex, $row[$field]);
    }

}