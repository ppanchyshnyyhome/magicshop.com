<?php
/**
 * Used in creating options for Yes|No config value selection
 *
 */
class Develop_Loader_Model_Type_Type
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('loader')->__('Recursively')),
            array('value' => 0, 'label'=>Mage::helper('loader')->__('One Level')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('loader')->__('One Level'),
            1 => Mage::helper('loader')->__('Recursively'),
        );
    }

}
