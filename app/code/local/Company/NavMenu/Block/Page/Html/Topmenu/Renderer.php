<?php
/* 
 * Rewrite class which renders template for Topmenu
 */

class Company_NavMenu_Block_Page_Html_Topmenu_Renderer extends Mage_Page_Block_Html_Topmenu_Renderer
{
    public function getAttributesValue(Varien_Data_Tree_Node $item, $field)
    {
        $categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('*');
        
        foreach ($categories as $category) {
            if($category->getId() == $item->getCatId()){
                $value = Mage::getResourceModel('catalog/category')
                        ->getAttributeRawValue($category->getId(), 
                                $field, 
                                Mage::app()->getStore()->getId()
                        );
            }
        }
            
        return $value;
    }

    /**
     * Generates string with all attributes that should be present in menu item element
     *
     * @param Varien_Data_Tree_Node $item
     * @return string
     */
    protected function _getRenderedMenuItemAttributes(Varien_Data_Tree_Node $item)
    {
        $html = '';
        $attributes = $this->_getMenuItemAttributes($item);
        
        if($this->getAttributesValue($item, 'nav_menu_additional_item_classes')){
            $itemClasses = $this->getAttributesValue($item, 'nav_menu_additional_item_classes');
            $itemClasses = explode(",", trim($itemClasses));
            $itemClasses = implode('', $itemClasses);
        } else{
            $itemClasses = '';
        }

        foreach ($attributes as $attributeName => $attributeValue) {
            $html .= ' ' . $attributeName . '="' . str_replace('"', '\"', $attributeValue).' '.$itemClasses.'"';
        }

        return $html;
    }
}