<?php
/* 
 * Observer to rewrite logic of making TopMenu
 */
class Company_NavMenu_Model_Rewrite_Observer extends Mage_Catalog_Model_Observer
{
    public function addCatalogToTopmenuItems(Varien_Event_Observer $observer) {
        $block = $observer->getEvent()->getBlock();
        $block->addCacheTag(Mage_Catalog_Model_Category::CACHE_TAG);
        
        $menu = $observer->getMenu();
        $tree = $menu->getTree();
        
        // Create my native node
        $node = new Varien_Data_Tree_Node(array(
                'name'   => Mage::helper('shares')->__('Shop'),
                'id'     => 'shop',
                'url'    => Mage::getUrl(),
        ), 'id', $tree, $menu);

        $menu->addChild($node);  
        
        $this->_addCategoriesToMenu(
            Mage::helper('catalog/category')->getStoreCategories(), $node, $block, true
        );
    }
    
    protected function _addCategoriesToMenu($categories, $parentCategoryNode, $menuBlock, $addTags = false)
    {
        $categoryModel = Mage::getModel('catalog/category');
        foreach ($categories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }

            $nodeId = 'category-node-' . $category->getId();

            $categoryModel->setId($category->getId());
            if ($addTags) {
                $menuBlock->addModelTags($categoryModel);
            }

            $tree = $parentCategoryNode->getTree();
            $categoryData = array(
                'name' => $category->getName(),
                'id' => $nodeId,
                'cat_id' => $category->getId(),
                'url' => Mage::helper('catalog/category')->getCategoryUrl($category),
                'is_active' => $this->_isActiveMenuCategory($category)
            );
            $categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $parentCategoryNode);
            $parentCategoryNode->addChild($categoryNode);

            $flatHelper = Mage::helper('catalog/category_flat');
            if ($flatHelper->isEnabled() && $flatHelper->isBuilt(true)) {
                $subcategories = (array)$category->getChildrenNodes();
            } else {
                $subcategories = $category->getChildren();
            }

            $this->_addCategoriesToMenu($subcategories, $categoryNode, $menuBlock, $addTags);
        }
    }
}