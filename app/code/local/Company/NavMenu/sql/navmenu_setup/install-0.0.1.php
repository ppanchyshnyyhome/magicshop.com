<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$setup->addAttributeGroup('catalog_category', 'Default', 'Additional Nav Menu Settings', 1000);

$setup->addAttribute('catalog_category', 'nav_menu_new_column_after', array(
        'group' => 'Additional Nav Menu Settings',
        'type' => 'int',
        'input' => 'select',
        'label' => 'New Column After',
        'source' => 'eav/entity_attribute_source_boolean',
        'required' => 0,
        'sort_order' => '5',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
))
    ->addAttribute('catalog_category', 'nav_menu_output_link', array(
        'group' => 'Additional Nav Menu Settings',
        'type' => 'int',
        'input' => 'select',
        'label' => 'Output Link',
        'source' => 'eav/entity_attribute_source_boolean',
        'required' => 0,
        'sort_order' => '6',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
))
    ->addAttribute('catalog_category', 'nav_menu_additional_item_classes', array(
        'group' => 'Additional Nav Menu Settings',
        'type' => 'varchar',
        'input' => 'text',
        'label' => 'Item Classes',
        'source' => 'eav/entity_attribute_source_boolean',
        'required' => 0,
        'sort_order' => '7',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
))
    ->addAttribute('catalog_category', 'nav_menu_replacement_content', array(
        'type' => 'text',
        'label' => 'Replacement Content',
        'input' => 'textarea',
        'required' => false,
        'sort_order' => '8',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'group' => 'Additional Nav Menu Settings',
));

$setup->updateAttribute('catalog_category', 'nav_menu_replacement_content', 'is_wysiwyg_enabled', 1);
$setup->updateAttribute('catalog_category', 'nav_menu_replacement_content', 'visible_on_front', 1);
$setup->updateAttribute('catalog_category', 'nav_menu_replacement_content', 'is_html_allowed_on_front', 1);
$installer->endSetup();