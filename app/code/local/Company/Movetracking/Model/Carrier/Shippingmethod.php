<?php
/**
 * My Tracking shipping model
 *
 * @package Company_Movetracking
 */

class Company_Movetracking_Model_Carrier_Shippingmethod 
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    /**
    * Unique internal shipping method identifier
    *
    * @var string [a-z0-9_]
    */
    protected $_code = 'movetracking';
    
    /**
     * Maximum Products Weight and Price in order
     * 
     * @var string [a-z0-9_]
     */
    protected $_settings = 'settings';

    

    /**
    * Collect rates for this shipping method based on information in $request
    *
    * @param Mage_Shipping_Model_Rate_Request $data
    * @return Mage_Shipping_Model_Rate_Result
    */
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        
        // Skip if not enabled
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active')) {
            return false;
        }
        
	/** @var Mage_Shipping_Model_Rate_Result $result */
	$result = Mage::getModel('shipping/rate_result');
 
	// Get max weight of order and price from configuration
	$values = unserialize(Mage::getStoreConfig('carriers/'.$this->_code.'/'.$this->_settings ,'max_weight'));
        echo '<pre>';
        print_r($values);
        echo '</pre>';
        
        // Define Delivery method
        $movetrackingDelivery = false;

	if($request->getPackageWeight() > $values) {
            $movetrackingDelivery = true;
	}
        
        if($movetrackingDelivery){
            $result->append($this->_getDeliveryRate());
        }
 
	return $result;
    }
    
    protected function _getDeliveryRate()
    {
        /** @var Mage_Shipping_Model_Rate_Result_Method $rate */
        $rate = Mage::getModel('shipping/rate_result_method');

        // Set Carrier Title
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        
        // Set Method and his Title
        $rate->setMethod('special');
        $rate->setMethodTitle('Special delivery');
        
        // Count the Price and Cost
        $rate->setPrice($values);
        $rate->setCost(0);

        return $rate;
    }
	 
    /**
    * This method is used when viewing / listing My Shipping Method
    */
    public function getAllowedMethods()
    {
        return array($this->_code => $this->getConfigData('name'));
    }
}