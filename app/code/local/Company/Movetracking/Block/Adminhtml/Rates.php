<?php
/*
 * Frontend Model for my Shipping Method
 */
class Company_Movetracking_Block_Adminhtml_Rates extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {
        $this->addColumn('max_weight', array(
            'label' => Mage::helper('movetracking')->__('Max Weight'),
            'style' => 'width:120px',
        ));
        $this->addColumn('price', array(
            'label' => Mage::helper('movetracking')->__('Price'),
            'style' => 'width:120px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('movetracking')->__('Add New Pairs');
        parent::__construct();
    }
}