<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'Mage/Checkout/controllers/CartController.php';
class Fastercheck_Ajaxcheckout_IndexController extends Mage_Checkout_CartController
{
    public function addAction() 
    {
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        if(($params['isAjax'] == 1) && ($this->_initProduct()->getTypeId() == 'simple')){
            $response = array();
            try {
                if (isset($params['qty'])) {
                    $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                    );
                    $params['qty'] = $filter->filter($params['qty']);
                }
 
                $product = $this->_initProduct();
                $related = $this->getRequest()->getParam('related_product');
 
                /**
                 * Check product availability
                 */
                if (!$product) {
                    $response['status'] = 'ERROR';
                    $response['message'] = $this->__('Unable to find Product ID');
                }
 
                $cart->addProduct($product, $params);
                if (!empty($related)) {
                    $cart->addProductsByIds(explode(',', $related));
                }
 
                $cart->save();
 
                $this->_getSession()->setCartWasUpdated(true);
 
                /**
                 * @todo remove wishlist observer processAddToCart
                 */
                Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
                );
 
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
                    $response['status'] = 'SUCCESS';    
                    $response['message'] = $message;
                    //New Code Here
                    $this->loadLayout();
                    $headminicart    = $this->getLayout()->getBlock('minicart_head')->toHtml();
                    $wrapperminicart = $this->getLayout()->getBlock('minicart_content')->toHtml();
                    Mage::register('referrer_url', $this->_getRefererUrl());
                    
                    $toplink = $this->getLayout()->getBlock('top.links')->toHtml();
                    if($sidebar_block = $this->getLayout()->getBlock('cart_sidebar')){
                        $sidebar = $sidebar_block->toHtml();
                    }
                    $response['toplink'] = $toplink;
                    $response['sidebar'] = $sidebar;
                    
                    $response['type_instance'] = $this->_initProduct()->getTypeId();
                    
                    // Set quantity of cart
                    $response['qty'] = $this->_getCart()->getSummaryQty();
                    // Set header minicart
                    $response['head']    = $headminicart;
                    // Set minicart content
                    $response['content'] = $wrapperminicart;
                }
            } catch (Mage_Core_Exception $e) {
                $msg = "";
                if ($this->_getSession()->getUseNotice(true)) {
                    $msg = $e->getMessage();
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        $msg .= $message.'<br/>';
                    }
                }
 
                $response['status'] = 'ERROR';
                $response['message'] = $msg;
            } catch (Exception $e) {
                $response['status'] = 'ERROR';
                $response['message'] = $this->__('Cannot add the item to shopping cart.');
                Mage::logException($e);
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;
        }else{
           $this->_redirect('*/*/');
        }
    }
}