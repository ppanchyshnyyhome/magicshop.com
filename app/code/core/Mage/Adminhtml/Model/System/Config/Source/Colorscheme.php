<?php
class Mage_Adminhtml_Model_System_Config_Source_Colorscheme
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('Light')),
            array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('Dark')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('adminhtml')->__('Dark'),
            1 => Mage::helper('adminhtml')->__('Light'),
        );
    }

}

